/*
 * BOOT_Peripherals.cpp
 *
 * Created: 12.05.2021 9:54:17
 *  Author: janh
 */ 

#include "BOOT_Peripherals.h"

#define USART0_BAUD_RATE(BAUD_RATE) ((float)F_CPU * 64 / (16 * (float)BAUD_RATE))

void UART0_Init(float Baud)
{
	PORTA.DIRSET = PIN0_bm; // TX pin as output
	PORTA.OUTCLR = PIN0_bm; //

	PORTA.DIRCLR = PIN1_bm; // RX pin set as input
	PORTA_PIN1CTRL &= ~PORT_PULLUPEN_bm; // Leave it as pulled-off

	USART0.BAUD = (uint16_t)USART0_BAUD_RATE(Baud); /* set baud rate register */

	USART0.CTRLB |= USART_TXEN_bm | USART_RXEN_bm;

	USART0.CTRLC = USART_CMODE_ASYNCHRONOUS_gc /* Asynchronous Mode */
	| USART_CHSIZE_8BIT_gc /* Character size: 8 bit */
	| USART_PMODE_DISABLED_gc /* No Parity */
	| USART_SBMODE_1BIT_gc; /* 1 stop bit */
}

void UART0_SendChar(char x){
	while((USART0.STATUS & USART_DREIE_bm) == 0);
	USART0.TXDATAL = x;
}

void UART0_SendStr(char* str){
	for (int i = 0; str[i] != 0; i++){
		UART0_SendChar(str[i]);
	}
}

char UART0_readChar()
{
	_PROTECTED_WRITE(WDT.CTRLA, WDT_PERIOD_8KCLK_gc);
	while (!(USART0.STATUS & USART_RXCIF_bm)){;}
	_PROTECTED_WRITE(WDT.CTRLA, 0);
	return USART0.RXDATAL;
}

void Display_Init(){
	PORTB.DIRSET = 0b00111111;
	PORTA.DIRSET = 0b11000100;
	PORTC.DIRSET = 0x3;
	PORTC.OUTCLR = 0x3;
	VREF.DAC0REF = 0x5;
	DAC0.DATA = 0x3DC2;
	DAC0.CTRLA |= DAC_OUTEN_bm | DAC_ENABLE_bm;
	uint8_t Comds[] = {0x01, 0x02, 0x0C, 0x3F};
	for(unsigned i=0; i<sizeof(Comds); i++){
		Display_SendCommand(Comds[i]);
		_delay_ms(100);
	}
	PORTA.OUTSET = _BV(2);
	
	Display_Clear();
	_delay_ms(20);
}

void Display_Clear(){
	Display_SendCommand(1);
	_delay_ms(50);
	Display_SendCommand(2);
	_delay_ms(50);
}

void Display_ClockPulse(){
	_delay_ms(1);
	PORTC.OUTSET = _BV(0);
	_delay_ms(1);
	PORTC.OUTCLR = _BV(0);
}

void Display_outByte(uint8_t val){
	PORTB.OUTCLR = 0b00111111;
	PORTB.OUTSET = (0b00111111 & val);
	PORTA.OUTCLR = 0b11000000;
	PORTA.OUTSET = (0b11000000 & val);
}

void Display_SendCommand(uint8_t Cmd){
	Display_outByte(Cmd);
	PORTC.OUTCLR = _BV(1);
	Display_ClockPulse();
	_delay_ms(1);
}

void Display_WriteStr(int Line, const char* in){
	Display_MoveToLine(Line);
	int i=0;
	while(in[i]!=0){
		if(in[i] == '\n'){
			Line++;
			Display_MoveToLine(Line);
			i++;
		}
		Display_WriteChar(in[i]);
		i++;
	}
}

void Display_MoveToLine(uint8_t Line){
	if(Line == 1){
		Display_MoveCursor(0x00);
		}else if(Line == 2){
		Display_MoveCursor(0x40);
		}else if(Line == 3){
		Display_MoveCursor(0x14);
		}else if(Line == 4){
		Display_MoveCursor(0x54);
		}else{
		Display_MoveCursor(0x00);
	}
}

void Display_MoveCursor(uint8_t Addr){
	Display_SendCommand(0x80 | (Addr & 0x7F));
}

void Display_WriteChar(char in){
	Display_outByte(in);
	PORTC.OUTSET = _BV(1);
	Display_ClockPulse();
}