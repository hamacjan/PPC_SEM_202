#include "BOOT_Peripherals.h"

void Set24MHz(){
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);
	_PROTECTED_WRITE(CLKCTRL.OSCHFCTRLA, 0x24);
}

void ReadPage(unsigned PageN, uint8_t* Array){
	uint16_t* ArrayD = (uint16_t*) Array;
	uint16_t* Base = (uint16_t*)(0x8000 + 16*512);
	if(PageN >= 16 && PageN <= 63){
		NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION0_gc;
		Base = (uint16_t*)(0x8000 + PageN*512);
		}else if(PageN >= 64 && PageN <= 127){
		NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION1_gc;
		Base = (uint16_t*)(0x8000 + (PageN-64)*512);
		}else if(PageN >= 128 && PageN <= 191){
		NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION2_gc;
		Base = (uint16_t*)(0x8000 + (PageN-128)*512);
		}else if(PageN >= 192 && PageN <= 254){
		NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION3_gc;
		Base = (uint16_t*)(0x8000 + (PageN-192)*512);
	}

	for(int i=0; i<256; i++){
		ArrayD[i] = Base[i];
	}
}

void ReadLastPage(uint8_t* Array){
	NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION3_gc;
	uint8_t* Base = (uint8_t*)(0x8000 + 63*512);
	for(int i=0; i<512; i++){
		Array[i] = Base[i];
	}
}

void WriteLastPage(uint8_t* Array){
	NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION3_gc;
	uint8_t* Base = (uint8_t*)(0x8000 + 63*512);
	
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_FLPER_gc);
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
	Base[0] = 0;
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
		
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_NOOP_gc);
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
	
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_FLWR_gc);
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
	for(int i=0; i<512; i++){
		Base[i] = Array[i];
		while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
	}
	
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_NOOP_gc);
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
}

int WritePage(unsigned PageN, uint8_t* Array){
	uint16_t* ArrayD = (uint16_t*) Array;
	uint16_t* Base = (uint16_t*)(0x8000 + 16*512);
	if(PageN >= 16 && PageN <= 63){
		NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION0_gc;
		Base = (uint16_t*)(0x8000 + PageN*512);
	}else if(PageN >= 64 && PageN <= 127){
		NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION1_gc;
		Base = (uint16_t*)(0x8000 + (PageN-64)*512);
	}else if(PageN >= 128 && PageN <= 191){
		NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION2_gc;
		Base = (uint16_t*)(0x8000 + (PageN-128)*512);
	}else if(PageN >= 192 && PageN <= 254){
		NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION3_gc;
		Base = (uint16_t*)(0x8000 + (PageN-192)*512);
	}
	if(NVMCTRL.STATUS & 0xF0) return 1;
	
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_FLPER_gc);
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
	Base[0] = 0;
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);

	if(NVMCTRL.STATUS & 0xF0) return 2;
	
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_NOOP_gc);
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
	if(NVMCTRL.STATUS & 0xF0) return 3;
	
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_FLWR_gc);
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
	for(int i=0; i<256; i++){
		while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
		Base[i] = ArrayD[i];
	}
	if(NVMCTRL.STATUS & 0xF0) return 5;
	
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_NOOP_gc);
	while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
	if(NVMCTRL.STATUS & 0xF0) return 6;
	
	return 0;
}

uint8_t ReadLastPageByte(uint16_t Addrr){
	NVMCTRL.CTRLB = NVMCTRL_FLMAP_SECTION3_gc;
	uint8_t* Base = (uint8_t*)(0x8000 + 63*512);
	return  Base[Addrr];
}

void WriteLastPageByte(uint16_t Addrr, uint8_t Value){
	uint8_t Buff[512];
	ReadLastPage(Buff);
	Buff[Addrr] = Value;
	WriteLastPage(Buff);
}

int main(void)
{
	Set24MHz();
	PORTC.OUTCLR = _BV(6);
	PORTD.OUTCLR = _BV(4);
	PORTD.OUTCLR = _BV(1);
	PORTC.PIN6CTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm;
	PORTD.PIN4CTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm;
	PORTD.PIN1CTRL = PORT_PULLUPEN_bm;
	_delay_ms(100);
	
	if(!(PORTC.IN & _BV(6) && PORTD.IN & _BV(4))){
		uint8_t Status = ReadLastPageByte(0x1FF);
		if(Status == 0x00){
			NVMCTRL.CTRLB = NVMCTRL_BOOTRP_bm;
			asm volatile("jmp 0x2000");
		}else if(Status == 0xFF){
			Display_Init();
			Display_WriteStr(1, "     !!NO FW!!    ");
			_PROTECTED_WRITE(WDT.CTRLA, WDT_PERIOD_2KCLK_gc);
		}else if(Status == 0x01){
			Display_Init();
			Display_WriteStr(1, "FAILED PROGRAMMING");
			Display_WriteStr(2, "     FLASH FW     ");
			_PROTECTED_WRITE(WDT.CTRLA, WDT_PERIOD_2KCLK_gc);
		}else{
			Display_Init();
			Display_WriteStr(1, "   CORRUPTED FW   ");
			Display_WriteStr(2, "    REFLASH FW    ");
			_PROTECTED_WRITE(WDT.CTRLA, WDT_PERIOD_2KCLK_gc);
		}
		while (1){};
	}
	
	UART0_Init(115200);
	Display_Init();
	Display_WriteStr(1, "Waiting Confirm     ");
	_PROTECTED_WRITE(WDT.CTRLA, WDT_PERIOD_8KCLK_gc);
	while(PORTD.IN & _BV(1));
	_PROTECTED_WRITE(WDT.CTRLA, 0);
		
	Display_WriteStr(1, "BOOTLOADER RUNNING  ");
	WriteLastPageByte(0x1FF, 0x01);
	uint8_t RecvHeader[513];
	
	for (int i = 0; i < 513; i++)	//Receive Header
	{
		RecvHeader[i] = UART0_readChar();
	}
	
	bool err = false;
	
	uint8_t SendHeader[513] = {0, 'O', 'K'};
		
	if(RecvHeader[0]>0){
		Display_WriteStr(1, "    UNSUPPORTED     ");
		err = true;
		SendHeader[1] = 'U';
		SendHeader[2] = 'N';
		SendHeader[3] = 'S';
		SendHeader[4] = 'U';
		SendHeader[5] = 'P';
	}
	
	if(RecvHeader[1] != 'P' || RecvHeader[2] != 'R' || RecvHeader[3] != 'O' || RecvHeader[4] != 'G'){
		Display_WriteStr(1, "    BAD HEADER      ");
		err = true;
		SendHeader[1] = 'H';
		SendHeader[2] = 'E';
		SendHeader[3] = 'A';
		SendHeader[4] = 'D';
		SendHeader[5] = 'E';
		SendHeader[6] = 'R';
	}
		
	for (int i = 0; i < 513; i++)	//Send Header
	{
		 UART0_SendChar(SendHeader[i]);
	}
	
	if(err){
		_delay_ms(5000);
		_PROTECTED_WRITE(RSTCTRL.SWRR, 1);
	}
	
	bool Running = true;
	while(Running){
		uint8_t DataBuffer[513];
		for (int i = 0; i < 513; i++)	//Receive data
		{
			DataBuffer[i] = UART0_readChar();
		}
		uint8_t WrtStat = 0;
		if (DataBuffer[0] < 16 || DataBuffer[0] == 0xFF)	//Check valid page
		{
			DataBuffer[0]++;
		}else{	//Looks like valid page
			char Buff[32];
			int perc = (DataBuffer[0]-16)/239.0*100;
			sprintf(Buff, "Page:   0x%02X %3d%%   ", DataBuffer[0], perc);
			Display_WriteStr(3, Buff);
			
			WrtStat = WritePage(DataBuffer[0], DataBuffer+1);
			ReadPage(DataBuffer[0], DataBuffer+1);
		}

		for (int i = 0; i < 513; i++)	//Send back buffer
		{
			UART0_SendChar(DataBuffer[i]);
		}

		uint8_t Ret = UART0_readChar();
		char Buff[32];
		sprintf(Buff, "Status: 0x%02X 0x%02X   ", Ret, WrtStat);
		Display_WriteStr(4, Buff);
		if(Ret == 0){
			UART0_SendChar(Ret);
			UART0_SendChar(WrtStat);
		}else if (Ret == 1) //End of data
		{
			WriteLastPageByte(0x1FF, 0x00);
			Running = false;
			UART0_SendChar(Ret);
			UART0_SendChar(WrtStat);
			Display_WriteStr(1, "      FINISHED      ");
			_delay_ms(2500);
			_PROTECTED_WRITE(RSTCTRL.SWRR, 1);
		}else{
			Display_WriteStr(1, " BOOTLOADER ERROR   ");
			Running = false;
			UART0_SendChar(Ret);
			UART0_SendChar(WrtStat);
			while(true);
		}
	}
	
	while (1){};
}