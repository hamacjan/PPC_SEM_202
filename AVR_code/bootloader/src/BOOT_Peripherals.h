/*
 * BOOT_Peripherals.h
 *
 * Created: 12.05.2021 9:54:32
 *  Author: janh
 */ 
#define F_CPU 24000000

#include <avr/io.h>
#include <avr/xmega.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/fuse.h>

#ifndef _BV
#define _BV(n) (1 << n)
#endif

#ifndef BOOT_PERIPHERALS_H_
#define BOOT_PERIPHERALS_H_

void UART0_Init(float Baud);
void UART0_SendChar(char x);
void UART0_SendStr(char* str);
char UART0_readChar();

void Display_Init();
void Display_Clear();
void Display_ClockPulse();
void Display_outByte(uint8_t val);
void Display_SendCommand(uint8_t Cmd);
void Display_WriteStr(int Line, const char* in);
void Display_MoveToLine(uint8_t Line);
void Display_MoveCursor(uint8_t Addr);
void Display_WriteChar(char in);

#endif /* BOOT_PERIPHERALS_H_ */