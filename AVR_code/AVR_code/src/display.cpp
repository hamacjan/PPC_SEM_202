/*
 * display.cpp
 *
 * Created: 18.03.2021 10:10:33
 *  Author: janh
 */ 

#include "display.h"

Display::Display(){
	PORTB.DIRSET = 0b00111111;
	PORTA.DIRSET = 0b11000100;
	PORTC.DIRSET = 0x3;
	PORTC.OUTCLR = 0x3;
	VREF.DAC0REF = 0x5;
	DAC0.DATA = 0x3DC2;
	DAC0.CTRLA |= DAC_OUTEN_bm | DAC_ENABLE_bm;
	uint8_t Comds[] = {0x01, 0x02, 0x0C, 0x3F};
	for(unsigned i=0; i<sizeof(Comds); i++){
		SendCommand(Comds[i]);
		_delay_ms(100);
	}
	
	TCB0.CTRLB |= TCB_CCMPEN_bm | TCB_CNTMODE_PWM8_gc;
	TCB0.CCMPL = 0xFF;
	TCB0.CCMPH = 0xFE;
	TCB0.CTRLA |= 1;
	
	Clear();
	_delay_ms(20);
}

void Display::Clear(){
	SendCommand(1);
	_delay_ms(50);
	SendCommand(2);
	_delay_ms(50);
}

void Display::WriteStr(int Line, const char* in){
	MoveToLine(Line);
	int i=0;
	while(in[i]!=0){
		if(in[i] == '\n'){
			Line++;
			MoveToLine(Line);
			i++;
		}
		WriteChar(in[i]);
		i++;
	}
}

void Display::BLSet(int perc){
	TCB0.CCMPH = (uint8_t)0xFF*((float)perc/100);
}

void Display::Contrast(uint16_t Cont){
	DAC0.DATA = Cont;
}

inline void Display::ClockPulse(){
	_delay_ms(1);
	PORTC.OUTSET = _BV(0);
	_delay_ms(1);
	PORTC.OUTCLR = _BV(0);
}

inline void Display::outByte(uint8_t val){
	PORTB.OUTCLR = 0b00111111;
	PORTB.OUTSET = (0b00111111 & val);
	PORTA.OUTCLR = 0b11000000;
	PORTA.OUTSET = (0b11000000 & val);
}

void Display::SendCommand(uint8_t Cmd){
	outByte(Cmd);
	PORTC.OUTCLR = _BV(1);
	ClockPulse();
	_delay_ms(1);
}

void Display::MoveToLine(uint8_t Line){
	if(Line == 1){
		MoveCursor(0x00);
	}else if(Line == 2){
		MoveCursor(0x40);
	}else if(Line == 3){
		MoveCursor(0x14);
	}else if(Line == 4){
		MoveCursor(0x54);
	}else{
		MoveCursor(0x00);
	}
}

void Display::MoveCursor(uint8_t Addr){
	SendCommand(0x80 | (Addr & 0x7F));
}

void Display::WriteChar(char in){
	outByte(in);
	PORTC.OUTSET = _BV(1);
	ClockPulse();
}