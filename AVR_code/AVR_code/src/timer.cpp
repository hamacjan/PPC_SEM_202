/*
 * timer.cpp
 *
 * Created: 21.04.2021 23:36:32
 *  Author: janh
 */ 
#include "includes.h"

void TCAn_Setup(TCA_t& TCA, float timeout){
	TCA.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV1024_gc;
	TCA.SINGLE.INTCTRL = TCA_SINGLE_OVF_bm;
	TCA.SINGLE.PER = (register16_t)(F_CPU/1024)*timeout;
}

void enableTCA_n(TCA_t& TCA){
	TCA.SINGLE.CTRLA |= TCA_SINGLE_ENABLE_bm;
}

void disableTCA_n(TCA_t& TCA){
	TCA.SINGLE.CTRLA &= ~TCA_SINGLE_ENABLE_bm;
	TCA.SINGLE.CNT = 0;
}

void resetTCA_n(TCA_t& TCA){
	TCA.SINGLE.CNT = 0;
}
