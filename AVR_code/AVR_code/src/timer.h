/*
 * timer.h
 *
 * Created: 21.04.2021 23:36:17
 *  Author: janh
 */ 

#ifndef TIMER_H_
#define TIMER_H_

void TCAn_Setup(TCA_t& TCA, float timeout);
void enableTCA_n(TCA_t& TCA);
void disableTCA_n(TCA_t& TCA);
void resetTCA_n(TCA_t& TCA);
ISR(TCA0_OVF_vect);

#endif /* TIMER_H_ */