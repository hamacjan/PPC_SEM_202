/*
 * UART.cpp
 *
 * Created: 18.03.2021 10:48:44
 *  Author: janh
 */ 
#include "includes.h"

#define USART0_BAUD_RATE(BAUD_RATE) ((float)F_CPU * 64 / (16 * (float)BAUD_RATE))

char UART_BUFFER[UART_BUFFER_SIZE];
bool BufferFresh = false;

void UART0_Init(float Baud)
{
		PORTA.DIRSET = PIN0_bm; // TX pin as output
		PORTA.OUTCLR = PIN0_bm; //

		PORTA.DIRCLR = PIN1_bm; // RX pin set as input
		PORTA_PIN1CTRL &= ~PORT_PULLUPEN_bm; // Leave it as pulled-off

		USART0.BAUD = (uint16_t)USART0_BAUD_RATE(Baud); /* set baud rate register */
		
		USART0.CTRLA |= USART_RXCIE_bm;
		
		USART0.CTRLB |= USART_TXEN_bm | USART_RXEN_bm;

		USART0.CTRLC = USART_CMODE_ASYNCHRONOUS_gc /* Asynchronous Mode */
		| USART_CHSIZE_8BIT_gc /* Character size: 8 bit */
		| USART_PMODE_DISABLED_gc /* No Parity */
		| USART_SBMODE_1BIT_gc; /* 1 stop bit */
}

void UART0_SendChar(char x){
	while((USART0.STATUS & USART_DREIE_bm) == 0);
	USART0.TXDATAL = x;
}

void UART0_SendStr(char* str){
	for (int i = 0; str[i] != 0; i++){
		UART0_SendChar(str[i]);
	}
}

char UART0_readChar()
{
	_PROTECTED_WRITE(WDT.CTRLA, WDT_PERIOD_8KCLK_gc);
	while (!(USART0.STATUS & USART_RXCIF_bm)){;}
	_PROTECTED_WRITE(WDT.CTRLA, 0);
	return USART0.RXDATAL;
}
uint8_t UART0_FillBuffer(char* buff){
	cli();
	if(!BufferFresh){
		sei();
		return 0;
	}
	strcpy(buff, UART_BUFFER);
	BufferFresh = false;
	sei();
	return 1;
}

ISR(USART0_RXC_vect){
	uint8_t index = 0;
	char c = 0;
	while (c != '\n' && c != '\r')
	{
		c = UART0_readChar();
		if(c != '\n' && c != '\r')
		{
			UART_BUFFER[index++] = c;
			if(index > UART_BUFFER_SIZE-1)
			{
				break;
			}
		}
	}
	UART_BUFFER[index] = '\0';
	BufferFresh = true;
}

int USART0_printChar_F(char character, FILE *stream)
{
	UART0_SendChar(character);
	return 0;
}

int USART0_getChar_F(FILE *stream)
{
	return UART0_readChar();
}

FILE* x = fdevopen(&USART0_printChar_F, &USART0_getChar_F);