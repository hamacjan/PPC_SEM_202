/*
 * display.h
 *
 * Created: 18.03.2021 10:02:42
 *  Author: janh
 */ 

#include "includes.h"

#ifndef DISPLAY_H_
#define DISPLAY_H_

#define Display_Delay F_CPU

class Display
{
public:
	Display();
	void Clear();
	void WriteChar(char in);
	void WriteStr(int Line, const char* in);
	void BLSet(int perc);
	void Contrast(uint16_t Cont);
	void MoveCursor(uint8_t Addr);
	void MoveToLine(uint8_t Line);
private:
	void SendCommand(uint8_t Cmd);
	inline void outByte(uint8_t val);
	inline void ClockPulse();
};

#endif /* DISPLAY_H_ */