/*
 * AVR_code.cpp
 *
 * Created: 18.03.2021 9:56:25
 * Author : janh
 */ 

#include "includes.h"

void Set24MHz(){
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);
	_PROTECTED_WRITE(CLKCTRL.OSCHFCTRLA, 0x24);
}

PotData_t* PotArr;
int counter = 0;

int main(void)
{
	Set24MHz();
	
//On SW restart, init counter on 1, to not trigger another reset.
	if(RSTCTRL.RSTFR & RSTCTRL_SWRF_bm){
		RSTCTRL.RSTFR = RSTCTRL_SWRF_bm;
		counter = 1;
	}
	
    TCAn_Setup(TCA1, 2);
	UART0_Init(115200);
	
	Display DisplayObj;
	DAC0.DATA = (uint16_t)(0xFFFF*0.35);
    char UartBuffer[UART_BUFFER_SIZE+1];
	char* UartData = UartBuffer+1;
	char OutBuffer[UART_BUFFER_SIZE];
	PotArr = InitPots();
	DisplayObj.BLSet(100);
	DisplayObj.WriteStr(1, "       WAITING ");
	DisplayObj.WriteStr(2, "          ON   ");
	DisplayObj.WriteStr(3, "         HOST  ");
	DisplayObj.WriteStr(4, "          SW   ");
	enableTCA_n(TCA1);
	sei();
    while (1){
#ifdef EncTest
	char Buff[20];
	sprintf(Buff, "%d   x", PotArr[0].Val);
	DisplayObj.WriteStr(1, Buff);
#endif
	    if(UART0_FillBuffer(UartBuffer)!=1){
		    _delay_ms(1);
			continue;
	    }
		strncpy(OutBuffer, "Unknown", UART_BUFFER_SIZE);
		switch (*UartBuffer)
		{
		case 'D':{
			DisplayObj.WriteStr(UartData[0]-'0', UartData+1);
			enableTCA_n(TCA1);
			}
		case 'S':{
			sprintf(OutBuffer, "%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d", 
				counter,
				PotArr[0].BtnPres,
				PotArr[1].BtnPres,
				PotArr[2].BtnPres,
				PotArr[3].BtnPres,
				PotArr[4].BtnPres,
				PotArr[0].Val,
				PotArr[1].Val,
				PotArr[2].Val,
				PotArr[3].Val,
				PotArr[4].Val
			);
			PotArr[0].Clear();
			PotArr[1].Clear();
			PotArr[2].Clear();
			PotArr[3].Clear();
			PotArr[4].Clear();
		}break;
		case 'C':{
			DAC0.DATA = (uint16_t)(0xC0FF*(float)atoi(UartData)/100);
			sprintf(OutBuffer, "ContSet %u", (unsigned)DAC0.DATA);
			}break;
		case 'B':{
			int Bl = atoi(UartData);
			DisplayObj.BLSet(Bl);
			sprintf(OutBuffer, "BLSet %d", Bl);
			}break;
		case 'R':{
			printf("RESETTING\n");
			while(!(USART0.STATUS & USART_DREIF_bm));
			_delay_ms(10);
			_PROTECTED_WRITE(RSTCTRL.SWRR, 1);
			}break;
		case 'I':{
				uint8_t* IDArr = (uint8_t*)&(SIGROW.SERNUM0);
				printf("V01-0x");
				for(int i=0; i<16; i++){
					printf("%02X", IDArr[i]);
				}
				OutBuffer[0] = 0;
			}break;
		}
		printf("%s\n", OutBuffer);
    }
}

ISR(TCA1_OVF_vect){
	TCA1.SINGLE.INTFLAGS = 1;
	counter++;
}
