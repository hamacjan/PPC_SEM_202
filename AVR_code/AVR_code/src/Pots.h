/*
 * Pots.h
 *
 * Created: 28.04.2021 21:37:52
 *  Author: janh
 */ 

#include "includes.h"

#ifndef POTS_H_
#define POTS_H_

struct PotData_t{
	int Val;
	bool FlagA, FlagB;
	int BtnPres;
	int BtnFlag;
	bool PotUp;
	PotData_t(){
		Val = 0;
		FlagA = false;
		FlagB = false;
		PotUp = false;
		BtnFlag = false;
		BtnPres = 0;
	}
	void ClerFlags(){
		FlagA = false;
		FlagB = false;
		PotUp = false;
	}
	void Clear(){
		Val = 0;
		BtnPres = 0;
	}
};

PotData_t* InitPots();

bool CheckInter(PORT_t& _PORT, uint8_t _mask);

#endif /* POTS_H_ */