/*
 * includes.h
 *
 * Created: 18.03.2021 10:45:37
 *  Author: janh
 */ 


#ifndef INCLUDES_H_
#define INCLUDES_H_

#define F_CPU 24000000

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <avr/sleep.h>

#ifndef _BV
#define _BV(n) (1 << n)
#endif

#define UART_BUFFER_SIZE 200

#include "Pots.h"
#include "timer.h"
#include "display.h"
#include "UART.h"
#include "Utils.h"

#endif /* INCLUDES_H_ */