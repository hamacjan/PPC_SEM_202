/*
 * UART.h
 *
 * Created: 18.03.2021 10:47:24
 *  Author: janh
 */ 
#include "includes.h"

#ifndef UART_H_
#define UART_H_

void UART0_Init(float Baud);

void UART0_SendChar(char x);

void UART0_SendStr(char* str);

char UART0_readChar();
uint8_t UART0_FillBuffer(char* buff);


#endif /* UART_H_ */