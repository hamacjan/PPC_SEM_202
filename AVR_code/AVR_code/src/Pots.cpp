/*
* Potscpp.cpp
*
* Created: 28.04.2021 21:37:37
*  Author: janh
*/

#include "Pots.h"

PotData_t Pots[5];

PotData_t* InitPots(){
	TCAn_Setup(TCA0, 0.05);
	enableTCA_n(TCA0);
	PORTC.PIN4CTRL = 0b00001001;	//4_A
	PORTC.PIN5CTRL = 0b00001001;    //4_B
	PORTC.PIN6CTRL = 0b00001000;    //4_Bt
	PORTC.PIN7CTRL = 0b00001001;    //5_A
	PORTD.PIN0CTRL = 0b00001001;    //5_B
	PORTD.PIN1CTRL = 0b00001000;	//5_Bt
	PORTD.PIN2CTRL = 0b00001001;	//1_A
	PORTD.PIN3CTRL = 0b00001001;	//1_B
	PORTD.PIN4CTRL = 0b00001000;	//1_Bt
	PORTE.PIN1CTRL = 0b00001001;	//3_A
	PORTE.PIN2CTRL = 0b00001001;	//3_B
	PORTE.PIN3CTRL = 0b00001000;	//3_Bt
	PORTF.PIN0CTRL = 0b00001001;	//2_A
	PORTF.PIN1CTRL = 0b00001001;	//2_B
	PORTF.PIN2CTRL = 0b00001000;	//2_Bt
	return Pots;
}

bool CheckInter(PORT_t& PORT, uint8_t mask){
	if(PORT.INTFLAGS & mask){
		_delay_us(70);
		resetTCA_n(TCA0);
		PORT.INTFLAGS = mask;
		return true;
	}else{
		return false;
	}
}

ISR(PORTC_PORT_vect){
	if(CheckInter(PORTC, _BV(4))){ //A
		if(Pots[0].PotUp) return;
		if(Pots[0].FlagB){
			Pots[0].Val--;
			Pots[0].FlagB = false;
			Pots[0].PotUp = true;
		}else{
			Pots[0].FlagA = true;
		}
		return;
	}
	if(CheckInter(PORTC, _BV(5))){ //B
		if(Pots[0].PotUp) return;
		if(Pots[0].FlagA){
			Pots[0].Val++;
			Pots[0].FlagA = false;
			Pots[0].PotUp = true;
		}else{
			Pots[0].FlagB = true;
		}
		return;
	}
	if(CheckInter(PORTC, _BV(7))){ //A
		if(Pots[1].PotUp) return;
		if(Pots[1].FlagB){
			Pots[1].Val--;
			Pots[1].FlagB = false;
			Pots[1].PotUp = true;
		}else{
			Pots[1].FlagA = true;
		}
		return;
	}
}

ISR(PORTD_PORT_vect){
	if(CheckInter(PORTD, _BV(0))){ //B
		if(Pots[1].PotUp) return;
		if(Pots[1].FlagA){
			Pots[1].Val++;
			Pots[1].FlagA = false;
			Pots[1].PotUp = true;
		}else{
			Pots[1].FlagB = true;
		}
		return;
	}
	if(CheckInter(PORTD, _BV(2))){ //A
		if(Pots[2].PotUp) return;
		if(Pots[2].FlagB){
			Pots[2].Val--;
			Pots[2].FlagB = false;
			Pots[2].PotUp = true;
		}else{
			Pots[2].FlagA = true;
		}
		return;
	}
	if(CheckInter(PORTD, _BV(3))){ //B
		if(Pots[2].PotUp) return;
		if(Pots[2].FlagA){
			Pots[2].Val++;
			Pots[2].FlagA = false;
			Pots[2].PotUp = true;
		}else{
			Pots[2].FlagB = true;
		}
		return;
	}
}

ISR(PORTE_PORT_vect){
	if(CheckInter(PORTE, _BV(1))){ //A
		if(Pots[3].PotUp) return;
		if(Pots[3].FlagB){
			Pots[3].Val--;
			Pots[3].FlagB = false;
			Pots[3].PotUp = true;
		}else{
			Pots[3].FlagA = true;
		}
		return;
	}
	if(CheckInter(PORTE, _BV(2))){ //B
		if(Pots[3].PotUp) return;
		if(Pots[3].FlagA){
			Pots[3].Val++;
			Pots[3].FlagA = false;
			Pots[3].PotUp = true;
		}else{
			Pots[3].FlagB = true;
		}
		return;
	}
}

ISR(PORTF_PORT_vect){
	if(CheckInter(PORTF, _BV(0))){ //A
		if(Pots[4].PotUp) return;
		if(Pots[4].FlagB){
			Pots[4].Val--;
			Pots[4].FlagB = false;
			Pots[4].PotUp = true;
		}else{
			Pots[4].FlagA = true;
		}
		return;
	}
	if(CheckInter(PORTF, _BV(1))){ //B
		if(Pots[4].PotUp) return;
		if(Pots[4].FlagA){
			Pots[4].Val++;
			Pots[4].FlagA = false;
			Pots[4].PotUp = true;
		}else{
			Pots[4].FlagB = true;
		}
		return;
	}
}

ISR(TCA0_OVF_vect){
	TCA0.SINGLE.INTFLAGS = TCA_SINGLE_OVF_bm;	//Reset Interrupt flag
	
	Pots[0].ClerFlags();		//Reset Flags
	Pots[1].ClerFlags();
	Pots[2].ClerFlags();
	Pots[3].ClerFlags();
	Pots[4].ClerFlags();
	
	if((PORTC.IN & _BV(6))==0){		//4_Btn
		if(Pots[0].BtnFlag==false){
			Pots[0].BtnPres++;
			Pots[0].BtnFlag=true;
		}
	}else{
		Pots[0].BtnFlag=false;
	}

	if((PORTD.IN & _BV(1))==0){		//5_Btn
		if(Pots[1].BtnFlag==false){
			Pots[1].BtnPres++;
			Pots[1].BtnFlag=true;
		}
	}else{
		Pots[1].BtnFlag=false;
	}
	
	if((PORTD.IN & _BV(4))==0){		//1_Btn
		if(Pots[2].BtnFlag==false){
			Pots[2].BtnPres++;
			Pots[2].BtnFlag=true;
		}
	}else{
		Pots[2].BtnFlag=false;
	}
	
	if((PORTD.IN & _BV(3))==0){		//3_Btn
		if(Pots[3].BtnFlag==false){
			Pots[3].BtnPres++;
			Pots[3].BtnFlag=true;
		}
		}else{
		Pots[3].BtnFlag=false;
	}
	
	if((PORTF.IN & _BV(2))==0){		//2_Btn
		if(Pots[4].BtnFlag==false){
			Pots[4].BtnPres++;
			Pots[4].BtnFlag=true;
		}
		}else{
		Pots[4].BtnFlag=false;
	}
}