# Mixér hlasitosti pro Windows

Tato semestrální práce se zajímá návrhem HW a SW pro fyzický kontrolér hlasitosti jednotlivých procesů ve Windows.

## Požadavky

- Windows Vista a novější
- Python 3.6+

## Instalace

Pro instalaci stáhněte tento repozitář, následně nainstalujte potřebné balíčky

```PS
pip install -r requirements.txt
```

Následně připojte zařízení a spusťte program na pozadí

```PS
pythonw .\BackgroundProc\Background.py
```

## Ovládání

V liště aplikací se po spuštění objeví ikonka **AC**, po kliknutí pravím tlačítkem myši na tuto ikonku se objeví možnost `CONFIG`, toto tlačítko otevře nastavení, kde se dají nastavit jednotlivé aplikace a sériový port. Po nastavení uložíte nastavení kliknutím na tlačítko `save`.  
Na zařízení by se nyní měly objevit jednotlivé názvy aplikací a jejich procentuální hlasitost. Jednotlivými enkodéry nyní můžete ovládat hlasitosti nastavených aplikací. Stiskem enkodéru zároveň můžete zaslat mediální klávesy. Pro ukončení aplikace klikněte na ikonku aplikace a stikněte `EXIT`.

## Update FW

> Jakmile se potvrdí vstup do bootloaderu, aktuální program je neplatný

Pokud máte nově naprogramované zařízení, nebo poškozený program, můžete nahrát nový FW bez potřeby externího programátoru.

1. Podržte tlačítka **A** a **C** při připojení zařízení
2. Potvrďte vstup do bootloaderu tlačítkem **B**, zařízení nyní očekává nový FW.
3. Spusťte následující příkaz, kde COM_PORT označuje sériový port připojeného zařízení a PATH_TO_HEX je cesta ke zdrojovému HEX souboru, aktuální soubor je `AVR_code\AVR_code\Debug\AVR_code.hex`

```
python .\FW-UPDATE.py COM_PORT PATH_TO_HEX
```

4. SW se nyní začne instalovat, po instalaci se zařízení samo restartuje a je připraveno k použití.

## Instalace bootloaderu

Pokud se poruší bootloader, je potřeba nahrát nový bootloader. Nahrání nového bootloaderu se provádí přes rozhraní UPDI řídícího procesoru, které je dostupné na PICkit 4 kompatibilním konektoru na levé straně. Samotný program se nalézá v `AVR_code\bootloader\src`

## Hardware

Centrální procesor je [AVR128DB48](https://www.microchip.com/wwwproducts/en/AVR128DB48), který komunikuje s PC pomocí převodníku CH340G, samotné rotační enkodéry jsou PEC12R-4217F-S0012. Displej je generický 20x4 LCD displej kompatibilní s HD44780.

## Funkce bootloaderu

Pro ukládání stavu FW je použita poslední stránka FLASH paměti, nastavená jako [APPDATA sekce](https://ww1.microchip.com/downloads/en/DeviceDoc/AVR128DB28-32-48-64-DataSheet-DS40002247A.pdf#page=77). Tento segment se narozdíl od EEPROM nastaví na 0xFF při instalaci bootloaderu externím programátorem a bootloader tak může uchovávat stav o aktuálně nahraném programu. Pro samotný bootloader je vyhrazeno prvních 16 stránek (0-15 včetně). Aplikace začíná na prvním bytu 16. stránky, tedy byte adresa 0x2000.

Každý packet má velikost 513 bytů. První přenesený packet je hlavička, kde první byte označuje nejvyšší verzi SW na PC, Následuje zbytek hlavičky, který je pro verzi 0: `0x50 0x52 0x4F 0x47`, celkový packet o oznámení programování pro bootloader verze 0 je tedy `0x00 0x50 0x52 0x4F 0x47`="\0PROG"  
Zařízení odpoví zpět 513 Byty
| byty            | ASCII         | Význam |
| :-------------: |:-------------:| :----- |
| `0x004F4B`| "\0OK"        | Zařízení přijalo hlavičku a je připraveno k programování |
| `0x00554E535550`     | "\0UNSUP" | Zařízení nepodporuje tuto verzi bootloaderu |
| `0x00484541444552` | "\0HEADER" | Chybná hlavička |

Následuje až 239 packetů, kde každý může přenést jednu stránku, tedy 512B softwaru pro celkovou kapacitu 122368B.
Každý packet se skládá z prvního bytu, který určuje adresu packetu, 16-254 včetně. Následuje 512B stránky. Bootloader odpoví zpět všemi 513B, pokud se tyto packety liší, nastala chyba komunikace a je potřeba opakované zaslání packetu.  

Po přenesení a ověření packetu zašle PC bootloaderu jeden byte.
| byte | Význam |
| :--: |:------:|
| `0x00` | Vše OK |
| `0x01` | Konec přenosu |
| `0xFF` | Chyba komunikace, ukončení |

Bootloader odpoví dvěma byty, první byte je zopakování statusu, a druhý může indikovat případnou chybu programovací periférie, za standartní operace je tento byte 0x00.

Touto odpovědí je ukončen přenos packetu, a je očekáván další, nebo ukončení komunikace.

## Komunikace se zařízením

Samotné zařízení nezavísí na aplikaci na PC, pouze zobrazuje přijatá data na displej a uchovává změny v enkodérech a tlačítkách. Po odeslání aktuálního stavu se stav resetuje, zařízení teda neuchovává absolutní polohu enkodérů.  
Samotná komunikace probíhá formou dotazů na zařízení, kde každý dotaz má svůj identifikační znak, a zařízení na něj odpovídá.

| znak | příklad   | příklad odpovědi |Význam |
| :---------: |:---------:|:-------:|:-----:|
| `R`         | `"R\n"`   | `"RESETTING\n"` | Softwarový Restart |
| `B`         | `"B75\n"` | `\n"BLSet 75\n"` | Nastaví jas na procentuální hodnotu.|
| `C`         | `"C33\n"` | `\n"ContSet 33\n"` | Nastaví kontrast na procentuální hodnotu|
| `I`         | `"I\n"`   | `"V01-0x??????\n"` | Zašle zpět verzi softwaru a výrobní sériové číslo procesoru |
| `S`         | `"S\n"`   | `"50\|2\|0\|0\|0\|0\|-4\|0\|0\|0\|0\n"` | Zašle zpět aktuální stav, popsáno níže.|
| `Dn`        | `"D1OK\n"`| `"50\|2\|0\|0\|0\|0\|-4\|0\|0\|0\|0\n"` | Zapíše na n-tý řádek zprávu, v tomto příkladě zapíše na první řádek "OK", odpověď se shoduje s příkazem `S`|
| `jiné`      | `"XX\n"`  | `"Unknown\n"` | Neznámý příkaz |

### Dekódování aktuálního stavu

`RunTime|B1|B2|B3|B4|B5|E1|E2|E3|E4|E5`  
První položka `RunTime` je doba běhu, inkrementuje se každé 2 vteřiny.  
Dalších 5 hodnot (B1-B5) reprezentuje počet stisků tlačítek od posledního dotazu.  
Posledních 5 hodnot (E1-E5) reprezentuje změnu enkodérů od posledního dotazu.

## Změny HW

Oproti schématům je ve finální verzi několik změn.

- Byl použít převodník CH430G místo CY7C65213A-28PVXI

- Připojení z PF6 přesunuto na PF3

- LED nevyužita
