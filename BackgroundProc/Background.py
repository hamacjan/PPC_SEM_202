from AudioInterface import AudioInterface
import json
import os
import sys
import time
import pystray
import win32api
from win32con import VK_MEDIA_PLAY_PAUSE, VK_MEDIA_NEXT_TRACK, VK_MEDIA_PREV_TRACK, VK_VOLUME_MUTE, KEYEVENTF_EXTENDEDKEY
import serial
import traceback

CorrectFile = lambda x: os.path.dirname(sys.argv[0])+"\\"+x

try:
    with open(CorrectFile("config.json"), "r") as fileIn:
        Settings = json.load(fileIn)
except:
    Settings = {
        "Port": "NONE",
        "Values":{
            "1": "None",
            "2": "None",
            "3": "None",
            "4": "None"
        },
        "Br": 32,
        "Bl": 100,
        "DevID": "XXXXX",
        "DevSernum": "XXXXX"
    }
    with open(CorrectFile("config.json"), "w") as out:
        json.dump(Settings, out, indent=4)

LastEdit = os.stat(CorrectFile("config.json")).st_mtime

Running = True
PortObj = None

def OpenPort():
    global IsConnected
    if Settings["Port"] == "NONE":
        return None
    try:
        p = serial.Serial(Settings["Port"], 115200, timeout=2)
        return p
    except:
        return None

PortObj = OpenPort()

def SendComm(Comm: str) -> str:
    global PortObj
    if PortObj == None: #If there's no port opened, send "no action"
        PortObj = OpenPort()
        return "0|0|0|0|0|0|0|0|0|0"
    
    try:
        data = (Comm.replace("\n", "|")+"\n") #Send and recieve one line
        PortObj.write(data.encode("UTF-8"))
        print("-> "+data)
        ret = PortObj.readline().decode("UTF-8")
        print("<- "+ret)
    except Exception as e:
        print(e)
        PortObj = None
        traceback.print_tb(e.__traceback__)
        print("No Conn")
        return "0|0|0|0|0|0|0|0|0|0"
    return ret

def CallBack(icon, item): #Exit button function
    global Running
    Running = False
    icon.stop()

def PressKey(x): #media key press
    if x == "pause":
        ev = VK_MEDIA_PLAY_PAUSE
    elif x == "skip":
        ev = VK_MEDIA_NEXT_TRACK
    elif x == "prev":
        ev = VK_MEDIA_PREV_TRACK
    elif x == "mute":
        ev = VK_VOLUME_MUTE
    else:
        return
    win32api.keybd_event(ev, 0, KEYEVENTF_EXTENDEDKEY, 0)

AI = AudioInterface()

Volumes = { #Current state of volumes
    "1": 0.5,
    "2": 0.5,
    "3": 0.5,
    "4": 0.5,
    "M": 0.5
}

STEP = 0.05 #One step adds 5%
OldKey4L = '-' #Toggling 

OldVols = None
changed = False
HaltSettingUpdate = False

def UpdateDevInSettings(ret): #Save new Device id to config file.
    global HaltSettingUpdate
    try:
        ID = int(ret[1:3])
        Sernum = ret[6:]
        with open(CorrectFile("config.json"), "r") as fileIn:
            Settings = json.load(fileIn)
        Settings["DevID"] = ID
        Settings["DevSernum"] = Sernum
        with open(CorrectFile("config.json"), "w") as out:
            json.dump(Settings, out, indent=4)
        HaltSettingUpdate = True
    except:
        pass

booting = True

def StartupSeq(res: bool = False): #Initialization of device
    global OldVols, booting
    booting = True
    if res:
        SendComm("R") #Reset
        time.sleep(2)
        if PortObj != None:
            PortObj.flushInput()
    SendComm("B"+str(Settings["Bl"])) #Back light
    SendComm("C"+str(Settings["Br"])) #Brightness (Actually contrast...)
    ret = SendComm("I") #Info
    UpdateDevInSettings(ret)
    OldVols = None
    time.sleep(1.1)
    booting = False

StartupSeq(True)

def PollInput(settings):
    global OldStr, PortObj, Contrast, changed, OldVols, OldKey4L, Volumes, booting

    if booting: #If device is in boot, send "No update"
        return "0|0|0|0|0|0|0|0|0|0"

    if changed: #If config was changed, send new settings
        changed = False 
        StartupSeq()

    if int(str(int(time.time()/10))[-1]) < 5: #Toggle Last line / Master every 5s
        CurrKey4L = '4'
        if(settings["Values"]['4']=='None'):
            CurrKey4L = 'M'
    else:
        CurrKey4L = 'M'

    if(CurrKey4L != OldKey4L or OldVols!=Volumes) and OldVols != None: #Update of last line
        if (CurrKey4L == 'M' or (OldVols['M'] != Volumes['M'])) and (OldVols['4'] == Volumes['4']): 
            st = "{:16s}{:3d}%".format("Master", int(Volumes['M']*100))
            ret = SendComm("D4"+st)
        else:
            procName = "".join(settings["Values"]['4'].split(".")[:-1])
            st = "{:16s}{:3d}%".format(procName, int(Volumes['4']*100))
            ret = SendComm("D4"+st)
        OldKey4L = CurrKey4L

    if(OldVols!=Volumes): #Update 1-3 lines on update
        print("------------->DIFF")
        Done = False
        for key in settings["Values"].keys():
            if key == '4':
                continue
            procName = "".join(settings["Values"][key].split(".")[:-1])
            st = "{:16s}{:3d}%".format(procName, int(Volumes[key]*100))
            ret = SendComm("D"+key+st)
            print(ret)
            if int(ret.split("|")[0])==0:
                StartupSeq()
            else:
                Done = True
        if Done:
            OldVols = Volumes.copy()
    else:
        ret = SendComm("S")
    ret = ret.split("|")
    ret = "|".join(ret[1:])
    print(ret)
    return ret #Send back the change in state

def loop(icon): #main loop
    icon.visible = True
    global Settings, LastEdit, AI, Running, Volumes, changed, HaltSettingUpdate
    try:
        while Running:
            time.sleep(0.1)
            if HaltSettingUpdate: #Once update was handled, it can be cleared
                HaltSettingUpdate = False
                LastEdit = os.stat(CorrectFile("config.json")).st_mtime
            if os.stat(CorrectFile("config.json")).st_mtime > LastEdit: #Detect config update
                try:
                    with open(CorrectFile("config.json"), "r") as fileIn:
                        Settings = json.load(fileIn)
                        changed = True
                    LastEdit = os.stat(CorrectFile("config.json")).st_mtime
                except Exception as e:
                    print(str(e))
                    continue

            #update volumes
            for idx in Settings["Values"].keys():
                Volumes[idx] = AI.GetVolume(Settings["Values"][idx])

            Volumes["M"] = AI.GetVolume("Master")

            print(Volumes)

            Recv = PollInput(Settings) #Update device and get current state

            Split = Recv.split("|") #Data convertion
            if len(Split) != 10:
                continue

            Btn0 = int(Split[0])
            Btn1 = int(Split[1])
            Btn2 = int(Split[2])
            Btn3 = int(Split[3])
            Btn4 = int(Split[4])

            for i in range(Btn0):
                PressKey("prev")
            for i in range(Btn1):
                PressKey("pause")
            for i in range(Btn2):
                PressKey("skip")
            for i in range(Btn4):
                PressKey("mute")

            #If there were no changes to encoder, continue
            if Split[5] == '0' and Split[6] == '0' and Split[7] == '0' and Split[8] == '0' and Split[9] == '0':
                continue

            Volumes["M"] = AI.GetVolume("Master") #Get current volumes
            for idx in Settings["Values"].keys():
                Volumes[idx] = AI.GetVolume(Settings["Values"][idx])
                print(Volumes[idx])

            for i in range(5,9): #Update volumes
                Volumes[str(i-4)] += STEP * int(Split[i])

            Volumes["M"] += STEP * int(Split[9])

            AI.SetVolume("Master", Volumes["M"]) #Write volumes back
            for idx in Settings["Values"].keys():
                 AI.SetVolume(Settings["Values"][idx], Volumes[idx])

    except Exception as e:
        print(e)
        traceback.print_tb(e.__traceback__)
        icon.stop()

from PIL import Image
from pystray import Icon as icon, Menu as menu, MenuItem as item
icon('test', Image.open(CorrectFile("icon.png")), menu=menu(
    item('RELOAD', lambda y, x: StartupSeq(True)),
    item('CONFIG', lambda y, x: os.system(sys.executable+" "+CorrectFile('Configurator.py'))),
    item('EXIT', CallBack)
    )).run(setup=loop)
