import json

import os
import sys
CorrectFile = lambda x: os.path.dirname(sys.argv[0])+"\\"+x

from AudioInterface import AudioInterface #Get audio devices
devices = AI = AudioInterface().GetDevices()
devices.remove("Master")
devices.append("None")

import serial.tools.list_ports #Get COM ports
ComPorts = []
ListP = serial.tools.list_ports.comports()
for element in ListP:
    ComPorts.append(element.device)

def CallBack():
    Settings = {
        "Port": COMS.get(),
        "Values":{
            "1": variable1.get(),
            "2": variable2.get(),
            "3": variable3.get(),
            "4": variable4.get()
        },
        "Br": Bright.get(),
        "Bl": Bl.get(),
        "DevID": DevId,
        "DevSernum": DevSernum
    }
    
    with open(CorrectFile("config.json"), "w") as out:
        json.dump(Settings, out, indent=4)

import tkinter as tk
import tkinter.messagebox

master = tk.Tk()
master.title("Cnf")
photo = tk.PhotoImage(file = CorrectFile("icon.png"))
master.iconphoto(False, photo)

COMS = tk.StringVar(master)
COMS.set(ComPorts[0])

variable1 = tk.StringVar(master)
variable2 = tk.StringVar(master)
variable3 = tk.StringVar(master)
variable4 = tk.StringVar(master)

DevId = 0
DevSernum = "None"

Bright = tk.IntVar(master)
Bl = tk.IntVar(master)

try:
    with open(CorrectFile("config.json"), "r") as fileIn:
        Settings = json.load(fileIn)
        COMS.set(Settings["Port"])
        variable1.set(Settings["Values"]["1"])
        variable2.set(Settings["Values"]["2"])
        variable3.set(Settings["Values"]["3"])
        variable4.set(Settings["Values"]["4"])
        Bright.set(Settings["Br"])
        Bl.set(Settings["Bl"])
        DevSernum = Settings["DevSernum"]
        DevId = Settings["DevID"]
except Exception as e:
    print(str(e))
    variable1.set(devices[0])
    variable2.set("None")
    variable3.set("None")
    variable4.set("None")
    Bright.set(100)
    Bl.set(32)

w = tk.OptionMenu(master, COMS, *ComPorts)
w.grid(row=0, column=0, sticky="ew")
w = tk.OptionMenu(master, variable1, *devices)
w.grid(row=1, column=0, sticky="ew")
w = tk.OptionMenu(master, variable2, *devices)
w.grid(row=2, column=0, sticky="ew")
w = tk.OptionMenu(master, variable3, *devices)
w.grid(row=3, column=0, sticky="ew")
w = tk.OptionMenu(master, variable4, *devices)
w.grid(row=4, column=0, sticky="ew")
B = tk.Button(master, text ="SAVE", command = CallBack, width=10)
B.grid(row=0, column=1, sticky="nsew", rowspan=5)
w = tk.Scale(master, variable=Bright, from_=0, to=100, orient=tk.HORIZONTAL)
w.grid(row=5, column=0, sticky="ew")
w = tk.Scale(master, variable=Bl, from_=0, to=100, orient=tk.HORIZONTAL)
w.grid(row=6, column=0, sticky="ew")

B = tk.Button(master, text ="Device Vers", command = lambda: tk.messagebox.showinfo(title="Device Info", message=f"Dev Version: {DevId}\nDevSernum: {DevSernum}"))
B.grid(row=5, column=1, rowspan=2)

tk.mainloop()