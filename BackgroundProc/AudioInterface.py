"""
This is edited version of:
https://github.com/AndreMiras/pycaw/blob/develop/examples/audio_controller_class_example.py
"""
from __future__ import print_function
from pycaw.pycaw import AudioUtilities
from ctypes import POINTER, cast
from comtypes import CLSCTX_ALL
from pycaw.pycaw import AudioUtilities, IAudioEndpointVolume

class AudioController(object):
    def __init__(self, process_name):
        self.process_name = process_name
        self.volume = self.process_volume()

    def mute(self):
        sessions = AudioUtilities.GetAllSessions()
        for session in sessions:
            interface = session.SimpleAudioVolume
            if session.Process and session.Process.name() == self.process_name:
                interface.SetMute(1, None)

    def unmute(self):
        sessions = AudioUtilities.GetAllSessions()
        for session in sessions:
            interface = session.SimpleAudioVolume
            if session.Process and session.Process.name() == self.process_name:
                interface.SetMute(0, None)

    def process_volume(self):
        sessions = AudioUtilities.GetAllSessions()
        for session in sessions:
            interface = session.SimpleAudioVolume
            if session.Process and session.Process.name() == self.process_name:
                return interface.GetMasterVolume()

    def set_volume(self, decibels):
        sessions = AudioUtilities.GetAllSessions()
        for session in sessions:
            interface = session.SimpleAudioVolume
            if session.Process and session.Process.name() == self.process_name:
                # only set volume in the range 0.0 to 1.0
                self.volume = min(1.0, max(0.0, decibels))
                interface.SetMasterVolume(self.volume, None)

class AudioInterface():
    devices = ["Master"]
    def __init__(self):
        sessions = AudioUtilities.GetAllSessions() #Create array of all devices
        for session in sessions:
            if session.Process != None:
                name = session.Process.name()
                if name not in self.devices:
                    self.devices.append(name)

    def GetDevices(self): #Gell all aknown devices.
        return self.devices

    def GetVolume(self, name):
        self.__init__()
        if name not in self.devices:
            return 0
        if name != "Master":    #Master volume has different API from "processes" 
            Min = 1.1
            sessions = AudioUtilities.GetAllSessions()
            for session in sessions:
                interface = session.SimpleAudioVolume
                if session.Process and session.Process.name() == name: #There might be multiple instances of the same process...
                    vol = interface.GetMasterVolume()
                    if vol != None:
                        Min = min(vol, Min)
            if Min == 1.1:
                return 0
            return Min
        else:
            devices = AudioUtilities.GetSpeakers()
            interface = devices.Activate(
            IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
            volume = cast(interface, POINTER(IAudioEndpointVolume))
            return volume.GetMasterVolumeLevelScalar()

    def SetVolume(self, name, value):
        self.__init__()
        if name not in self.devices:
            return False
        if name != "Master": #Master volume has different API
            AC = AudioController(name)
            if value >= 1:
                AC.set_volume(1)
            elif value <= 0:
                AC.set_volume(0)
            else:
                AC.set_volume(value)
        else:
            if value < 0:
                value = 0
            elif value > 1:
                value = 1

            devices = AudioUtilities.GetSpeakers()
            interface = devices.Activate(
            IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
            volume = cast(interface, POINTER(IAudioEndpointVolume))
            volume.SetMasterVolumeLevelScalar(value, None)
        return True