import time
import sys
from builtins import bytes, int
from serial import Serial
from intelhex import IntelHex


flashsize = 122368 #Maximal size of App hex
baudrate = 115200

if len(sys.argv) != 3:
    print("Wrong number of arguments, use [COM] [FILE]")

comport = sys.argv[1]
file = sys.argv[2]

# Parse hex file
ih = IntelHex()
fileextension = file[-3:]
ih.loadfile(file, format=fileextension)
append = ih.maxaddr()
if (append > flashsize):
    print ("WRONG HEX FILE")
    sys.exit(1)
appstart = ih.minaddr()
app = ih.tobinarray(end=flashsize-1)

while(len(app)/512 != 239): #padding
    app.append(0xFF)

uart = Serial(comport, baudrate=baudrate, timeout=10)

#Generating FW Header
Header = [0] * 513
Header[0] = 0 #Header Version
Header[1] = ord('P')
Header[2] = ord('R')
Header[3] = ord('O')
Header[4] = ord('G')

uart.write(bytearray(Header))
recv = uart.read(513)

print(recv[0:10])

#Check FW Header
if recv[0] != 0 or recv[1] != ord('O') or recv[2] != ord('K'): 
    print("ERROR")
    exit(-1)

start = time.time()
print ("Uploading", len(app), "bytes...")
for i in range(239):    #There are 239 pages to send
    print ("Uploading page: ", i, "from 239 - ", int(i/239*100), "%")
    Base = i*512 #Base address of current page in source hex

    Array = [] #Generating page packet
    Array.append(i+16)
    for j in range(512):
        Array.append(app[Base + j])
    values = bytearray(Array)

    uart.write(values)
    recv = uart.read(513)
    if recv != values: #Reply check
        uart.write(bytes([0xFF]))
        print("ERR")
        recv = uart.read(2)
        print("Ret: ", recv)
        break
    elif i == 238:
        uart.write(bytes([0x01])) #End of source
        print("END")
    else:
        uart.write(bytes([0x00])) #All OK
    recv = uart.read(2)
    print("Ret: ", recv)

end = time.time()
print("Upload took", int(end-start), "seconds")